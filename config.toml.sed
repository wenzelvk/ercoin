s/^cors_allowed_origins = .*/cors_allowed_origins = ["*"]/
s/^timeout_commit = .*/timeout_commit = "3s"/
s/^create_empty_blocks = .*/create_empty_blocks = false/
s/^create_empty_blocks_interval = .*/create_empty_blocks_interval = "900s"/
s/^cache_size = .*/cache_size = 0/

%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-define(MAX_BLOCK_HEIGHT, 4294967295). %% 2^32 - 1
-define(MAX_UINT64, 16#ffffffffffffffff).
-define(MAX_EPOCH_LENGTH, 31536000). %% 1 year
-define(BLOCK_LENGTH, 3).
-define(ACCOUNT_EXTENSION_DAYS, 30).
-define(HASH(X), crypto:hash(sha256, X)).
-define(SETS, ordsets).

-define(OK, 0).
-define(BAD_REQUEST, 1).
-define(NOT_FOUND, 2).
-define(FORBIDDEN, 3).
-define(INSUFFICIENT_FUNDS, 4).
-define(INVALID_TIMESTAMP, 5).
-define(ALREADY_EXECUTED, 6).
-type error_code() :: ?OK | ?BAD_REQUEST | ?NOT_FOUND | ?FORBIDDEN | ?INSUFFICIENT_FUNDS | ?INVALID_TIMESTAMP | ?ALREADY_EXECUTED.

-type hash() :: <<_:256>>.
-type block_height() :: 1..?MAX_BLOCK_HEIGHT.
-type epoch_length() :: 2..?MAX_EPOCH_LENGTH.
-type money() :: 0..?MAX_UINT64.
-type voting_power() :: non_neg_integer().
-type choices() :: map().
-record(
   vote,
   {timestamp :: ercoin_timestamp:timestamp(),
    choices :: choices()}).
-type vote() :: #vote{}.
-record(
   account,
   {address= <<0:256>> :: ercoin_sig:address(),
    balance=0 :: money(),
    valid_until=1 :: ercoin_timestamp:timestamp(),
    locked_until=none :: ercoin_timestamp:timestamp() | none,
    validator_pk=none :: ercoin_sig:address() | none}).
-type account() :: #account{}.
-record(
   data,
   %% Some default values are provided to prevent Dialyzer from complaining in tests.
   {protocol=1 :: pos_integer(),
    epoch_length=604800 :: epoch_length(),
    epoch_stage=beginning :: ercoin_epoch:stage(),
    height=0 :: block_height() | 0,
    %% While we use one function to provide a default for the fields below, it doesn’t guarantee us to obtain the same value,
    %% so it is better to ensure this in a constructor than to rely on default field values.
    last_epoch_end=ercoin_timestamp:now() :: ercoin_timestamp:timestamp(),
    previous_timestamp=ercoin_timestamp:now() :: ercoin_timestamp:timestamp(),
    timestamp=ercoin_timestamp:now() :: ercoin_timestamp:timestamp(),
    %% We provide the field below to allow deterministic time progress in tests.
    now_fun=fun ercoin_timestamp:now/1 :: fun((data()) -> ercoin_timestamp:timestamp()),
    fee_deposit=0 :: money(),
    fresh_txs=?SETS:new() :: ?SETS:set(binary()),
    fresh_txs_hash= <<0:256>> :: hash(),
    accounts=gb_merkle_trees:empty() :: gb_merkle_trees:tree(),
    entropy_fun=fun ercoin_entropy:simple_entropy/1 :: fun ((data()) -> binary()),
    validators=gb_merkle_trees:empty() :: ercoin_validators:current(),
    future_validators :: ercoin_validators:future(),
    %% A copy of data used to maintain state for the purpose of checking transactions. It is important that checked transactions modify this state,
    %% so that a block proposer can provide a valid sequence of transactions.
    %%
    %% We could handle the mempool connection using a separate process, but it would mean copying the data between processes on each commit (when mempool state is resetted), while when storing it in the same process we can benefit from term sharing.
    mempool_data :: undefined | #data{}}).
-type data() :: #data{}.

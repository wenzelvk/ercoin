%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_account).

-export_type(
   [accounts/0]).

-export(
   [accounts_from_option_maps/1,
    add_balance/3,
    construct/1,
    deserialize/1,
    foldr/3,
    get/2,
    is_locked/1,
    lookup_balance/2,
    put/2,
    serialize/1]).

-compile({parse_transform, dynarec}).

-include_lib("include/ercoin.hrl").

-type accounts() :: gb_merkle_trees:tree().

-spec is_locked(account()) -> boolean().
is_locked(#account{locked_until=LockedUntil}) ->
    LockedUntil =/= none.

-spec serialize(account()) -> {Key :: binary(), Value :: binary()}.
serialize(
  #account{
     address=Address,
     balance=Balance,
     valid_until=ValidUntil,
     locked_until=LockedUntil,
     validator_pk=ValidatorPK}) ->
    {Address,
     case LockedUntil of
         none ->
             <<ValidUntil:4/unit:8, Balance:8/unit:8>>;
         _ ->
             <<ValidUntil:4/unit:8, Balance:8/unit:8, LockedUntil:4/unit:8, ValidatorPK/binary>>
     end}.

-spec deserialize({Key :: binary(), Value :: binary()}) -> account().
deserialize({Address, AccountBin}) ->
    <<ValidUntil:4/unit:8, Balance:8/unit:8, Rest/binary>> = AccountBin,
    BaseAccount =
        #account{
           address=Address,
           balance=Balance,
           valid_until=ValidUntil},
    case Rest of
        <<>> ->
            BaseAccount;
        <<LockedUntil:4/unit:8, ValidatorPK:32/binary>> ->
            BaseAccount#account{
              locked_until=LockedUntil,
              validator_pk=ValidatorPK}
    end.

-spec get(ercoin_sig:address(), data() | accounts()) -> account() | none.
get(Address, #data{accounts=Accounts}) ->
    get(Address, Accounts);
get(Address, Accounts) ->
    case gb_merkle_trees:lookup(Address, Accounts) of
        none ->
            none;
        AccountBin ->
            ercoin_account:deserialize({Address, AccountBin})
    end.

-spec put(account(), data()) -> data();
         (account(), accounts()) -> accounts().
put(Account, Data=#data{accounts=Accounts}) ->
    NewAccounts = ?MODULE:put(Account, Accounts),
    Data#data{accounts=NewAccounts};
put(Account, AccountsTree) ->
    {Address, AccountBin} = ercoin_account:serialize(Account),
    gb_merkle_trees:enter(Address, AccountBin, AccountsTree).

-spec add_balance(ercoin_sig:address(), money(), data()) -> data().
%% @doc Add balance to an account, creating it if necessary.
add_balance(Address, Value, Data=#data{timestamp=Timestamp}) ->
    NewAccount =
        case ercoin_account:get(Address, Data) of
            Account=#account{balance=Balance} ->
                Account#account{balance=Balance + Value};
            none ->
                #account{
                   address=Address,
                   valid_until=Timestamp,
                   balance=Value}
        end,
    ?MODULE:put(NewAccount, Data).

-spec lookup_balance(ercoin_sig:address(), data()) -> money().
%% @doc Return balance of an address, 0 if account does not exist.
lookup_balance(Address, Data) ->
    case ?MODULE:get(Address, Data) of
        #account{balance=Balance} ->
            Balance;
        none ->
            0
    end.

-spec foldr(fun ((account(), Acc) -> Acc), Acc, accounts()) -> Acc.
foldr(F, Acc0, Accounts) ->
    gb_merkle_trees:foldr(
      fun (AccountBin, Acc) ->
              Account = deserialize(AccountBin),
              F(Account, Acc)
      end,
      Acc0,
      Accounts).

-spec maybe_decode_b64(list() | binary()) -> binary().
maybe_decode_b64(B64) when is_list(B64) orelse byte_size(B64) rem 32 =/= 0 ->
    base64:decode(B64);
maybe_decode_b64(Bin) ->
    Bin.

-spec construct(map()) -> account().
%% Construct an account from a provided option map.
%% Options generally correspond to account fields, with some special cases implemented (see the code for details).
construct(OptionMap) ->
    Now = ercoin_timestamp:now(),
    maps:fold(
      fun (Key, Value, AccountAcc) ->
              case Key of
                  valid_for ->
                      set_value(valid_until, Now + Value, AccountAcc);
                  locked_for ->
                      set_value(locked_until, Now + Value, AccountAcc);
                  address ->
                      set_value(address, maybe_decode_b64(Value), AccountAcc);
                  validator_pk ->
                      set_value(validator_pk, maybe_decode_b64(Value), AccountAcc);
                  _ ->
                      set_value(Key, Value, AccountAcc)
              end
      end,
      #account{},
      OptionMap).

-spec accounts_from_option_maps(list(map())) -> accounts().
%% @doc Construct an account tree from provided list of constructor options.
%% @see construct/1
accounts_from_option_maps(OptMaps) ->
    lists:foldl(
      fun (OptMap, TreeAcc) ->
              ?MODULE:put(construct(OptMap), TreeAcc)
      end,
      gb_merkle_trees:empty(),
      OptMaps).

%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_abci).
-behaviour(gen_statem).
-behaviour(abci_app).
-export(
   [start_link/0]).

-compile({parse_transform, dynarec}).

%% gen_statem
-export(
   [callback_mode/0,
    code_change/4,
    handle_event/4,
    init/1,
    terminate/3]).

%% abci_app
-export([handle_request/1]).

-include_lib("abci_server/include/abci.hrl").
-include_lib("include/ercoin.hrl").

start_link() ->
    gen_statem:start_link({local, ?MODULE}, ?MODULE, [], []).

callback_mode() ->
    [handle_event_function, state_enter].

code_change(_OldVsn, OldState, OldData, _Extra) ->
    {ok, OldState, OldData}.

init(_Args) ->
    case ercoin_persistence:current_data() of
        {ok, Data} ->
            {ok, gossiping, Data};
        none ->
            {ok, uninitialized, none}
    end.

terminate(_Reason, _State, _Data) ->
    ok.

handle_event({call, From}, #'tendermint.abci.types.RequestCheckTx'{tx=MaybeTxBin}, _, Data=#data{mempool_data=MempoolData}) ->
    {ErrorCode, NewMempoolData} =
        ercoin_tx:handle_bin(
          MaybeTxBin,
          ercoin_data:apply_shift_to_now(MempoolData),
          ercoin_tx:default_age_margin(MempoolData) * 3 div 4),
    {keep_state, Data#data{mempool_data=NewMempoolData}, {reply, From, #'tendermint.abci.types.ResponseCheckTx'{code=ErrorCode}}};
handle_event({call, From}, #'tendermint.abci.types.RequestCommit'{}, committing, Data) ->
    Response = #'tendermint.abci.types.ResponseCommit'{data=ercoin_data:app_hash(Data)},
    Reply = {reply, From, Response},
    Actions =
        %% It is important that we dump data at such time that future_validators is not a promise.
        case ercoin_epoch:stage(Data) of
            drawing_announced ->
                [Reply, {next_event, internal, dump_data}];
            _ ->
                [Reply]
        end,
    {next_state, gossiping, Data, Actions};
handle_event(
  {call, From},
  #'tendermint.abci.types.RequestEndBlock'{},
  committing,
  Data=
      #data{
         epoch_stage=EpochStage,
         height=Height,
         validators=Validators,
         future_validators=FutureValidators}) ->
    Diffs =
        case EpochStage of
            drawing_to_be_announced ->
                calculate_diffs(FutureValidators, Validators);
            _ ->
                []
        end,
    Data1 = Data#data{height=Height + 1},
    {keep_state, Data1, {reply, From, #'tendermint.abci.types.ResponseEndBlock'{validator_updates=Diffs}}};
handle_event({call, From}, #'tendermint.abci.types.RequestDeliverTx'{tx=MaybeTxBin}, committing, Data)->
    {ErrorCode, NewData} = ercoin_tx:handle_bin(MaybeTxBin, Data),
    Events =
        case ErrorCode of
            ?OK ->
                [ercoin_event:to_abci(Event) || Event <- ercoin_tx:events(MaybeTxBin, Data)];
            _ ->
                []
        end,
    {keep_state, NewData, {reply, From, #'tendermint.abci.types.ResponseDeliverTx'{code=ErrorCode, events=Events}}};
handle_event({call, _}, _, committing, _) ->
    {keep_state_and_data, postpone};
handle_event(
  {call, From},
  #'tendermint.abci.types.RequestBeginBlock'{
     last_commit_info=#'tendermint.abci.types.LastCommitInfo'{votes=Votes},
     header=#'tendermint.abci.types.Header'{time=NewTimestampProto}},
  gossiping,
  Data=#data{timestamp=OldTimestamp, previous_timestamp=OldPreviousTimestamp}) ->
    NewTimestamp = ercoin_timestamp:from_protobuf(NewTimestampProto),
    ValidatorsWithPresence =
        [{ercoin_validators:key_value_by_tendermint_address(TendermintAddress, Data), Signed}
         || #'tendermint.abci.types.VoteInfo'{signed_last_block=Signed, validator=#'tendermint.abci.types.Validator'{address=TendermintAddress}} <- Votes],
    {Data1, Events} = ercoin_data:shift_to_timestamp(NewTimestamp, Data),
    ABCIEvents = [ercoin_event:to_abci(Event) || Event <- Events],
    NewData =
        ercoin_epoch:progress(
          Data1,
          OldTimestamp - OldPreviousTimestamp,
          ValidatorsWithPresence),
    {next_state, committing, NewData, {reply, From, #'tendermint.abci.types.ResponseBeginBlock'{events=ABCIEvents}}};
handle_event({call, From}, Query, gossiping, Data) when is_record(Query, 'tendermint.abci.types.RequestQuery') ->
    {keep_state_and_data, {reply, From, ercoin_query:perform(Query, Data)}};
handle_event({call, From}, #'tendermint.abci.types.RequestInfo'{}, gossiping, Data) ->
    Response =
        #'tendermint.abci.types.ResponseInfo'{
           last_block_app_hash=ercoin_data:app_hash(Data),
           last_block_height=Data#data.height},
    {keep_state_and_data, {reply, From, Response}};
handle_event(internal, dump_data, gossiping, Data) ->
    ok = ercoin_persistence:dump_data_async(Data),
    keep_state_and_data;
handle_event({call, From}, #'tendermint.abci.types.RequestInitChain'{app_state_bytes=AppStateJSON}, uninitialized, none) ->
    Data = #data{validators=Validators} = binary_to_term(base64:decode(jiffy:decode(AppStateJSON))),
    TendermintValidators = [#'tendermint.abci.types.ValidatorUpdate'{pub_key=#'tendermint.abci.types.PubKey'{data=PK, type="ed25519"}, power=Power} || {PK, <<Power, _/binary>>} <- gb_merkle_trees:to_orddict(Validators)],
    {next_state, gossiping, Data, {reply, From, #'tendermint.abci.types.ResponseInitChain'{validators=TendermintValidators}}};
handle_event({call, From}, #'tendermint.abci.types.RequestInfo'{}, uninitialized, _) ->
    Reply = #'tendermint.abci.types.ResponseInfo'{last_block_height=0, last_block_app_hash= <<>>},
    {keep_state_and_data, {reply, From, Reply}};
handle_event(enter, _, gossiping, Data) ->
    NewMempoolData = Data#data{mempool_data=undefined},
    {keep_state, Data#data{mempool_data=NewMempoolData}};
handle_event(enter, _, _, _) ->
    keep_state_and_data.

handle_request(Request) ->
    gen_statem:call(?MODULE, Request).

-spec calculate_diffs(gb_merkle_trees:tree(), gb_merkle_trees:tree()) -> list(#'tendermint.abci.types.ValidatorUpdate'{}).
calculate_diffs(New, Old) ->
    KeysFun =
        fun (Tree) ->
                gb_merkle_trees:foldr(
                  fun ({Key, _}, Acc) -> [Key|Acc] end,
                  [],
                  Tree)
        end,
    DeletedEntries = [#'tendermint.abci.types.ValidatorUpdate'{pub_key=#'tendermint.abci.types.PubKey'{data=PK, type="ed25519"}, power=0} || PK <- KeysFun(Old) -- KeysFun(New)],
    NewEntries =
        [begin
             <<VP, _/binary>> = Value,
             #'tendermint.abci.types.ValidatorUpdate'{pub_key=#'tendermint.abci.types.PubKey'{data=PK, type="ed25519"}, power=VP}
         end || {PK, Value} <- gb_merkle_trees:to_orddict(New) -- gb_merkle_trees:to_orddict(Old)],
    NewEntries ++ DeletedEntries.

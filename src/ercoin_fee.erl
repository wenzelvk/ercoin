%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_fee).

-export(
   [deserialize/1,
    div_ceil/2,
    fee/2,
    fees/1,
    serialize/1]).

-include_lib("include/ercoin.hrl").
-include_lib("include/ercoin_tx.hrl").

-spec serialize(map()) -> binary().
serialize(
  #{fee_per_tx := FeePerTx,
    fee_per_256_bytes := FeePer256B,
    fee_per_account_day := FeePerAccountDay}) ->
    <<FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8>>.

-spec deserialize(binary()) -> map().
deserialize(<<FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8>>) ->
    #{fee_per_tx => FeePerTx,
      fee_per_256_bytes => FeePer256B,
      fee_per_account_day => FeePerAccountDay}.

-spec div_ceil(integer(), integer()) -> integer().
%% @doc Divide integers with rounding up.
div_ceil(Dividend, Divisor) ->
    Dividend div Divisor + min(Dividend rem Divisor, 1).

-spec fee(tx(), data()) -> money().
fee(#vote_tx{}, _Data) ->
    0;
fee(Tx, Data) ->
    #{fee_per_256_bytes := FPer256B,
      fee_per_tx := FPerTx}
        = fees(Data),
    div_ceil(byte_size(ercoin_tx:serialize(Tx)) * FPer256B, 256) + FPerTx.

-spec fees(data()) -> map().
fees(Data) ->
    maps:remove(
      protocol,
      maps:merge(
        #{fee_per_256_bytes => 0,
          fee_per_account_day => 0,
          fee_per_tx => 0},
        voted_choices(Data))).

-spec merge_choices_vps(choices(), voting_power(), map()) -> map().
merge_choices_vps(SingleChoices, VP, ChoicesVPs) ->
    Keys = maps:keys(SingleChoices),
    lists:foldl(
      fun (Key, Acc) ->
              ChoiceVPsForKey = maps:get(Key, Acc, []),
              Acc#{Key => [{maps:get(Key, SingleChoices), VP}|ChoiceVPsForKey]}
      end,
      ChoicesVPs,
      Keys).

-spec voted_choices(data()) -> choices().
voted_choices(#data{validators=Validators}) ->
    {VoteSum, ChoicesVPs} =
        gb_merkle_trees:foldr(
          fun ({_, <<VP/integer, _:3/binary, VoteBin/binary>>}, {VPSum, ChoicesVPs}) ->
                  case ercoin_vote:deserialize(VoteBin) of
                      none ->
                          {VPSum, ChoicesVPs};
                      #vote{choices=Choices} ->
                          {VPSum + VP, merge_choices_vps(Choices, VP, ChoicesVPs)}
                  end
          end,
          {0, #{}},
          Validators),
    SortedChoicesVPs = maps:map(fun (_, ChoiceVPs) -> lists:sort(ChoiceVPs) end, ChoicesVPs),
    maps:map(
      %% The quantile function should return higher middle value if sum of weights is even, but
      %% to be even more sure we make the first argument a little higher than 0.5.
      fun (_, ChoiceVPs) -> quantiles:'weighted-quantile'(0.500001, ChoiceVPs, VoteSum) end,
      SortedChoicesVPs).

;; Licensed under the Apache License, Version 2.0 (the “License”);
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;     http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an “AS IS” BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(defmodule merkle_proofs_tests
  (export all))

(include-lib "triq/include/triq.hrl")

(defun merkle-path ()
  (triq_dom:list (tuple (oneof '(left right)) (triq_dom:binary 32))))

(defun invalid-path-bin ()
  (triq_dom:oneof
   (list
    (LET
     invalid-path-list
     (SUCHTHAT
      invalid-path-list
      (triq_dom:non_empty (triq_dom:list (list (triq_dom:byte) (triq_dom:binary 32))))
      (lists:any (match-lambda (((cons b _)) (> b 1))) invalid-path-list))
     (list_to_binary invalid-path-list))
    (SUCHTHAT
     invalid-bin
     (triq_dom:binary)
     (=/= (rem (byte_size invalid-bin) 33) 0))
    )))

(defun prop_path_serialization ()
  (FORALL
   path
   (merkle-path)
   (=:= (clj:-> path
            (merkle_proofs:serialize-path)
            (merkle_proofs:deserialize-path))
        path)))

(defun prop_invalid_bin_returns_none ()
  (FORALL
   invalid-bin
   (invalid-path-bin)
   (=:= 'none (merkle_proofs:deserialize-path invalid-bin))))

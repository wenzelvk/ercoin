%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_timestamp_tests).

-include_lib("include/ercoin_test.hrl").

prop_timestamp_to_iso_and_back_conversion() ->
    ?FORALL(
       Timestamp,
       pos_integer(),
       ercoin_timestamp:from_iso(ercoin_timestamp:to_iso(Timestamp)) =:= Timestamp).

prop_timestamp_to_protobuf_and_back_conversion() ->
    ?FORALL(
       Timestamp,
       pos_integer(),
       ercoin_timestamp:from_protobuf(ercoin_timestamp:to_protobuf(Timestamp)) =:= Timestamp).

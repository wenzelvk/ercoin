%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_query_gen).

-include_lib("abci_server/include/abci.hrl").
-include_lib("include/ercoin_test.hrl").

-export(
   [data_with_query/0,
    data_with_query_account_not_existing/0,
    data_with_query_account_valid/0,
    data_with_query_fees/0,
    data_with_query_unknown/0]).

data_with_query_unknown() ->
    {data(), #'tendermint.abci.types.RequestQuery'{path="non-existent", data= <<"foo">>}}.

data_with_query_account_valid() ->
    ?LET(
       {Data, Account},
       data_with_account(),
       {Data,
        #'tendermint.abci.types.RequestQuery'{
           path="account",
           data=Account#account.address}}).

data_with_query_account_not_existing() ->
    ?LET(
       {Data, Bin},
       {data(), binary(32)},
       {Data,
        #'tendermint.abci.types.RequestQuery'{
           path="account",
           data=Bin}}).

data_with_query_fees() ->
    ?LET(
       Data,
       data(),
       {Data, #'tendermint.abci.types.RequestQuery'{path="fees"}}).

data_with_query() ->
    oneof(
      [fun data_with_query_unknown/0,
       fun data_with_query_account_valid/0,
       fun data_with_query_account_not_existing/0,
       fun data_with_query_fees/0]).
